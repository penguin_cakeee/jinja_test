from jinja2 import Template


persons = [
    {"name": "Lera", "old": 22, "weight": 64.5},
    {"name": "Kirill", "old": 21, "weight": 72.3},
    {"name": "Vera", "old": 44, "weight": 65.0}
]

tpl = '''
{%- for u in users -%}
{% filter upper %}{{u.name}}{% endfilter %}
{% endfor -%}
'''

tm = Template(tpl)
msg = tm.render(users=persons)

print(msg)
