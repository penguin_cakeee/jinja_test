from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('learn_include')
env = Environment(loader=file_loader)

template = env.get_template('about.html')

output = template.render()
print(output)
