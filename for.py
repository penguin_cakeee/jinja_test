from jinja2 import Template


cities = [{'id': 1, 'city': 'Minsk'},
          {'id': 2, 'city': 'Omsk'},
          {'id': 3, 'city': 'Homel'},
          {'id': 4, 'city': 'Moscow'}]

link = '''<select name="cities">
{% for c in cities -%}
{% if c.id < 3 -%}
    <option value="{{c['id']}}">{{c['city']}}</option>
{% elif c.city == 'Moscow' -%}
    <option>{{c['city']}}</option>
{% else -%}
  {{c['city']}}
{% endif -%}
{% endfor -%}
</select>'''

tm3 = Template(link)
msg3 = tm3.render(cities=cities)

print(msg3)
