from jinja2 import Template


coffee = [{'name': 'espresso', 'price': 3},
          {'name': 'cappuccino', 'price': 7},
          {'name': 'latte', 'price': 8},
          {'name': 'americano', 'price': 5}]


tp = "Total price {{ cs | sum(attribute='price') }}"

tm = Template(tp)
msg = tm.render(cs=coffee)

print(msg)
