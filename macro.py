from jinja2 import Template


persons = [
    {"name": "Lera", "old": 22, "weight": 64.5},
    {"name": "Kirill", "old": 21, "weight": 72.3},
    {"name": "Vera", "old": 44, "weight": 65.0}
]

html = '''
{% macro input(name, value='', type='text', size=20) -%}
    <input type="{{ type }}" name="{{ name }}" value="{{ value|e }}" size="{{ size }}">
{%- endmacro %}

{{ input('username') }}
{{ input('email') }}
{{ input('password') }}
'''

html2 = '''
{% macro list_users(list_of_user) -%}
<ul>
{% for u in users -%}
    <li>{{u.name}}{{caller(u)}}
{% endfor %}
</ul>
{%- endmacro %}

{% call(user) list_users(users)%}
    <ul>
    <li>age: {{user.old}}
    <li>weight: {{user.weight}}
    </ul>
{% endcall -%}
'''

tm = Template(html)
msg = tm.render()

tm = Template(html2)
msg2 = tm.render(users=persons)

print(msg, msg2)
