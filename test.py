from jinja2 import Template, escape


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


per = Person('Lera', 21)
link = ''' <a href="#">Ссылка</a>'''

tm = Template(" Hi, my name is {{ p.name }}, my age is {{ p.age }} ")
msg = tm.render(p=per)

tm1 = Template("{{ link | e}}")
msg1 = tm1.render(link=link)

msg2 = escape(link)

print(msg, msg1, msg2, sep='\n')
