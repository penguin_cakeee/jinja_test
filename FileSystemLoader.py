from jinja2 import Environment, FileSystemLoader, FunctionLoader

persons = [
    {"name": "Lera", "old": 22, "weight": 64.5},
    {"name": "Kirill", "old": 21, "weight": 72.3},
    {"name": "Vera", "old": 44, "weight": 65.0}
]


def loadTpl(path):
    if path == "index":
        return '''Имя {{u.name}}, возраст {{u.old}}'''
    else:
        return '''Данные: {{u}}'''


file_loader = FunctionLoader(loadTpl)
env0 = Environment(loader=file_loader)

tm0 = env0.get_template('index')
msg0 = tm0.render(u=persons[0])


file_loader = FileSystemLoader('')
env = Environment(loader=file_loader)

tm = env.get_template('macro.html')
msg = tm.render(users=persons)

print(msg0, msg, sep='\n')
