from jinja2 import Environment, FileSystemLoader, FunctionLoader

persons = [
    {"name": "Lera", "old": 22, "weight": 64.5},
    {"name": "Kirill", "old": 21, "weight": 72.3},
    {"name": "Vera", "old": 44, "weight": 65.0}
]


file_loader = FileSystemLoader('learn_include')
env = Environment(loader=file_loader)

tm = env.get_template('page.html')
msg = tm.render(domain='http://lera.com', title='Lera')

tm2 = env.get_template('page2.html')
msg2 = tm2.render(domain='http://vera.com', title='Vera')

print(msg, msg2, sep='\n')
